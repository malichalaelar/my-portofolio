import React from "react";
import { withRouter } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography, Toolbar } from "@material-ui/core";
import GitHubIcon from "@material-ui/icons/GitHub";

const useStyles = makeStyles((theme) => ({
  appbar: {
    backgroundColor: "white",
    color: "black",
    width: "100%",
    position: "fixed",
    top: 0,
    boxShadow: "none",
    borderBottom: "1px solid #F1F2F6",
    height: "90px",
  },
  boxRepo: {
    width: "50%",
    height: "40px",
    borderRadius: "4px",
    backgroundColor: "#cc99cc",
  },
}));

function Component() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <AppBar position="fixed" className={classes.appbar}>
        <Grid container style={{ paddingTop: "30px" }}>
          <Grid item xs={2}>
            <Typography
              style={{
                textAlign: "center",
                fontWeight: "bold",
                color: "#cc99cc",
              }}
            >
              malicha
            </Typography>
          </Grid>
          <Grid
            item
            xs={8}
            style={{
              display: "flex",
              justifyContent: "center",
            }}
          >
            <Grid container>
              <Grid item xs={4}>
                <Typography style={{ textAlign: "right", color: "#cc99cc" }}>
                  About
                </Typography>
              </Grid>
              <Grid
                item
                xs={4}
                style={{ textAlign: "center", color: "#cc99cc" }}
              >
                <Typography>Blog</Typography>
              </Grid>
              <Grid item xs={4} style={{ textAlign: "left", color: "#cc99cc" }}>
                <Typography>Contact</Typography>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            xs={2}
            style={{
              display: "flex",
              justifyContent: "flex-start",
            }}
          >
            <div className={classes.boxRepo}>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-around",
                  paddingTop: "7px",
                }}
              >
                <GitHubIcon style={{ color: "white" }} />{" "}
                <Typography style={{ color: "white" }}>
                  My Repository
                </Typography>
              </div>
            </div>
          </Grid>
        </Grid>
      </AppBar>
    </React.Fragment>
  );
}

export default withRouter(Component);
