import React from "react";
import { withRouter } from "react-router-dom";
import Appbar from "../../component/appbar";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Photo from "../../assets/myPhoto.jpg";
import ReactLogo from "../../assets/react.svg";
import JsLogo from "../../assets/js.svg";
import Github from "../../assets/github.svg";

function Component(props) {
  return (
    <React.Fragment>
      <Appbar />
      <Grid container style={{ paddingTop: "10%", display: "flex" }}>
        <Grid xs={6}>
          <div style={{ width: "100%", paddingLeft: "13%", paddingTop: "6%" }}>
            <Typography
              style={{ fontWeight: "bold", fontSize: "50px", color: "#cc99cc" }}
            >
              Hi, I am Malicha
            </Typography>
            <Typography style={{ color: "#cc99cc", fontSize: "18px" }}>
              Front End Web Developer
            </Typography>
            <div
              style={{
                paddingTop: "10px",
                display: "flex",
              }}
            >
              <div
                style={{
                  height: "40px",
                  width: "14%",
                  border: "1px solid #cc99cc",
                  borderRadius: "6px",
                }}
              >
                <Typography
                  style={{
                    color: "#cc99cc",
                    textAlign: "center",
                    paddingTop: "9px",
                  }}
                >
                  About Me
                </Typography>
              </div>
              <div style={{ paddingLeft: "25px", width: "100%" }}>
                <div
                  style={{
                    height: "40px",
                    width: "14%",
                    backgroundColor: "#cc99cc",
                    borderRadius: "6px",
                  }}
                >
                  <Typography
                    style={{
                      color: "white",
                      textAlign: "center",
                      paddingTop: "9px",
                    }}
                  >
                    Look My CV
                  </Typography>
                </div>
              </div>
            </div>
          </div>
        </Grid>
        <Grid xs={6}>
          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            <img
              src={Photo}
              style={{
                width: "30%",
                borderRadius: "100%",
                marginRight: "14%",
              }}
            />
          </div>
        </Grid>
      </Grid>
      <div style={{ paddingTop: "25px" }}>
        <div
          style={{ width: "100%", backgroundColor: "#cc99cc", height: "400px" }}
        >
          <Grid container style={{ paddingTop: "8%" }}>
            <Grid
              item
              xs={4}
              style={{ display: "flex", justifyContent: "flex-end" }}
            >
              <img src={ReactLogo} style={{ width: "15%" }} />
            </Grid>
            <Grid
              item
              xs={4}
              style={{ display: "flex", justifyContent: "center" }}
            >
              <img src={JsLogo} style={{ width: "15%" }} />
            </Grid>
            <Grid item xs={4}>
              <img src={Github} style={{ width: "15%" }} />
            </Grid>
          </Grid>
        </div>
      </div>
    </React.Fragment>
  );
}

export default withRouter(Component);
