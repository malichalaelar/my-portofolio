import React from "react";
import { withRouter } from "react-router-dom";
import Appbar from "../../component/appbar";

function Component(props) {
  return (
    <React.Fragment>
      <Appbar />
    </React.Fragment>
  );
}

export default withRouter(Component);
